﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Net;
using System.IO;
using System.Json;

namespace BusinessLayer
{
    
    public class QRScanner : ContentPage
    {
        public async Task<ZXing.Result> Run_QR_scannerView()
        {
            var scanner = new ZXing.Mobile.MobileBarcodeScanner();
            return await scanner.Scan();

        }

        public async Task<JsonValue> GetProductDetailFromServer(string product_id)
        {
            string url = GlobalValues.GetProductDetailAPI(product_id);
                return await FetchProductDetails(url);
                //this.Content = newView.Content;
        }

        private async Task<JsonValue> FetchProductDetails(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }
    }
}
