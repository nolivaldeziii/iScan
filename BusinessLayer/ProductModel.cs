﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ProductModel
    {
    }

    public class ProductItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }

    }



    public class ProductItemList : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public ObservableCollection<ProductItem> _itemList;
        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged == null)
                return;
            PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public ObservableCollection<ProductItem> Items
        {
            get { return _itemList; }
            set { _itemList = value; OnPropertyChanged("Items"); }
        }
        public void ItemList(List<ProductItem> itemList)
        {
            Items = new ObservableCollection<ProductItem>();
            foreach (ProductItem itm in itemList)
            {
                Items.Add(itm);
            }
        }
    }
}

