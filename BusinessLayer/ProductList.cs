﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;

using Xamarin.Forms;

using System.Json;
using System.IO;
using System.Net;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class ProductList : ContentPage
    {

        public static string ProductListAPILink = GlobalValues.PRODUCT_ALL_API;

        public async Task<JsonValue> getProducttList()
        {

            string url = ProductListAPILink;

            return await FetchProductList(url);

            //return await GlobalValues.Manager_Product_save;

            //this.Content = newView.Content;
        }

        private async Task<JsonValue> FetchProductList(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }


        //public class fooItem
        //{
        //    public string Id { get; set; }
        //    public string Name { get; set; }
        //    public string Quantity { get; set; }
        //}
    }
}
