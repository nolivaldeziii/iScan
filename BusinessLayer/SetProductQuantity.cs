﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Json;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace BusinessLayer
{
    public class SetProductQuantity : ContentPage
    {
        //public static string addProductQuantity = "http://iscan2.azurewebsites.net/api/addproductquantity?productID=";
        //public static string deductProductQuantity = "http://iscan2.azurewebsites.net/api/deductproductquantity?productID=";
       
        public void fetchProductQuantity(string product_id, int Quantity)
        {
            //if (Quantity > initQuanity)
            //{
            //    string url = addProductQuantity + product_id;
            //    setProductQuantity(url, Quantity);
            //}
            //else
            //{
            //    string url = deductProductQuantity + product_id;
            //    setProductQuantity(url, Quantity);
            //}
            ////this.Content = newView.Content;
            //setProductQuantity(product_id, Quantity.ToString());
        }

        public async Task<JsonValue> setProductQuantity(string pid, string newQuantity)
        {
            string url = GlobalValues.UpdateProductAPI(pid, newQuantity);
            //HttpClient client = new HttpClient();
            //var uri = new Uri(url);
           
            //var content = new StringContent(
            //                JsonConvert.SerializeObject(new { Quantity = newQuantity }), Encoding.UTF8, "application/json");
            //await client.PutAsync(uri, content);


            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }
    }
}
