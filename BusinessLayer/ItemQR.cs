﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Json;

namespace BusinessLayer
{
    public enum QRTYPE { Item, Cart}

    public class ItemQR
    {
        private JsonValue JObj;
        public QRTYPE Type { get; set; }
        public List<object> Items;

        public static readonly string KEY_TYPE = "TYPE";
        public static readonly string KEY_IS_ITEM_CHECK = "ITEM";
        public static readonly string KEY_IS_CART_CHECK = "CART";

        public static readonly string KEY_ITEMS = "ITEMS";
              
        public static readonly string KEY_ID = "ID";
        public static readonly string KEY_NAME = "NAME";
        public static readonly string KEY_PINFO = "PRODUCT INFO";
        public static readonly string KEY_IMG = "IMAGE";

        public static readonly string KEY_QTY = "QTY";
        public static string GenerateCartJSON(List<Cart> TheCart)
        {
   
            string CartJsonArray = "";

            foreach(Cart i in TheCart)
            {
                CartJsonArray += GenerateJSONArray(i.Id,i.Name,i.Quantity);
                CartJsonArray += ",";
            }
            CartJsonArray = CartJsonArray.Remove(CartJsonArray.Length - 1);

            string CartJSONFormat = @"{"""+  KEY_TYPE  + @""": """ +
                                        KEY_IS_CART_CHECK + @""","""  + 
                                        KEY_ITEMS           + @""": ["  + 
                                        CartJsonArray           + "] }";

            CartJSONFormat = CartJSONFormat.Trim().ToUpper();

            return CartJSONFormat;
        }

        private static string GenerateJSONArray(string Id, string Name, string Quantity)
        {
            //return string.Format(@"{ ""Id"": ""{0}"",""Name"": ""{1}"",""QTY"": ""{2}""} ", Id,Name,Quantity);
            return (@"{ """+    KEY_ID      + @""": """+ Id         + @""","""+ 
                                KEY_NAME    + @""": """+ Name       + @""","""+ 
                                KEY_QTY     + @""": """+ Quantity   + @"""} ");
        }

        public ItemQR(string RawJObj)
        {
            Items = new List<object>();

            JObj = JsonObject.Parse(RawJObj);

            if(JObj[KEY_TYPE] == KEY_IS_ITEM_CHECK)
            {
                this.Type = QRTYPE.Item;
                ProcessItem();
            }else if(JObj[KEY_TYPE] == KEY_IS_CART_CHECK)
            {
                this.Type = QRTYPE.Cart;
                ProcessCart();
            }
            else
            {
                throw new System.ArgumentException("Invalid QR");
            }
           

        }
        /*
         * sample Json for qr generation
         * 
                {
                  "Type": "Item",
                  "Items": 
                  [
                    {"Id": "1",
                    "Name": "Product Name",
                    "Product Info": "Lorem Ipsum"
                    } 
                  ]
                }
         * 
         */
        private void ProcessItem()
        {   
            for(int i= 0; i < JObj[KEY_ITEMS].Count; i++)
            {
                JsonValue JChild = JObj[KEY_ITEMS][i];
                SingleItem newItem = new SingleItem();
                newItem.Id = JChild[KEY_ID];
                newItem.Name = JChild[KEY_NAME];
                newItem.Info = JChild[KEY_PINFO];
                //newItem.ImageURL = JChild[KEY_IMG];
                //JChild["ProductName"].ToString();

                Items.Add(newItem);
            }
        }

        private void ProcessCart()
        {
            for (int i = 0; i < JObj[KEY_ITEMS].Count; i++)
            {
                JsonValue JChild = JObj[KEY_ITEMS][i];
                Cart newItem = new Cart();
                newItem.Id = JChild[KEY_ID];
                newItem.Name = JChild[KEY_NAME];
                newItem.Quantity = JChild[KEY_QTY];

                Items.Add(newItem);
            }
        }


    }

    public class Cart
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Quantity { get; set; }
        public double Price { get; set; }
    }

    public class SingleItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public string ImageURL { get; set; }
    }

    //public class fooItem
    //{
    //    public string Id { get; set; }
    //    public string Name { get; set; }
    //    public int Quantity { get; set; }
    //}


}
