﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class GlobalValues
    {
        public static readonly string SITE_URL = "http://iscan2.azurewebsites.net";
        
        public static readonly string PRODUCT_ALL_API       = SITE_URL + "/api/getallproducts";
        public static readonly string PRODUCT_BEST_AMT_API  = SITE_URL + "/api/bestsellerbyamount";
        public static readonly string PRODUCT_BEST_QTY_API  = SITE_URL + "/api/bestsellerbyquantity";
        public static readonly string PRODUCT_REPORT_API    = SITE_URL + "/api/salesreport";
        
        private static readonly string PRODUCT_DETAIL_API   = SITE_URL + "/api/getproductdetails?productID={0}";
        private static readonly string PRODUCT_CHECKOUT_API = SITE_URL + "/api/checkoutproduct?prodId={0}&quantity={1}";

        private static readonly string PRODUCT_ADD_API      = SITE_URL + "/api/addproductquantity?productID={0}";
        private static readonly string PRODUCT_SUB_API      = SITE_URL + "/api/deductproductquantity?productID={0}";
        private static readonly string PRODUCT_UPDATE_API   = SITE_URL + "/api/updateproductquantity?productId={0}&quantity={1}";

        public static JsonValue Manager_Product_save;

        public static int MAX_CART_STRING_LENGTH = 60;
        public static int MAX_ADMIN_STRING_LENGTH = 40;

        public static string GetProductDetailAPI(string pid)
        {
            return string.Format((PRODUCT_DETAIL_API), pid);
        }

        public static string UpdateProductAPI(string pid, string qty)
        {
            return string.Format(PRODUCT_UPDATE_API, pid, qty);
        }

        public static string CheckoutProductAPI(string pid,string qty)
        {
            return string.Format(PRODUCT_CHECKOUT_API, pid, qty);
        }

        public static string GetAddProductAPI(string pid)
        {
            return string.Format((PRODUCT_ADD_API), pid);
        }

        public static string GetSubtractProductAPI(string pid)
        {
            return string.Format((PRODUCT_SUB_API), pid);
        }
    }
}
