using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using iScan.Manager.Dependency;
using System.Json;
using System.Threading.Tasks;

using System.Net;
using System.IO;
using BusinessLayer;

namespace iScan.Manager.Droid.Dependency
{
    public class GetProduct : Java.Lang.Object, IGetProducts
    {
        public async Task<JsonValue> getProducttList()
        {
            return await FetchProductList(GlobalValues.PRODUCT_ALL_API);
        }

        private async Task<JsonValue> FetchProductList(string url)
        {
            // Create an HTTP web request using the URL:
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));
            request.ContentType = "application/json";
            request.Method = "GET";

            // Send the request to the server and wait for the response:
            using (WebResponse response = await request.GetResponseAsync())
            {
                // Get a stream representation of the HTTP web response:
                using (Stream stream = response.GetResponseStream())
                {
                    // Use this stream to build a JSON document object:
                    JsonValue jsonDoc = await Task.Run(() => JsonObject.Load(stream));
                    //Console.Out.WriteLine("Response: {0}", jsonDoc.ToString());

                    // Return the JSON document:
                    return jsonDoc;
                }
            }
        }
    }
}