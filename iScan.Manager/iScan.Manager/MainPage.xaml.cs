﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

using BusinessLayer;
using System.Json;
using System.Collections.ObjectModel;

namespace iScan.Manager
{
    public partial class MainPage : ContentPage
    {
        //List<fooItem> prodList;
        ObservableCollection<ProductItem> ObserveableProductList;
        //List<fooItem> prodList = new List<fooItem>();


        viewProduct newView; // to store quantity :)

        public ProductItemList listItem;
        public MainPage()
        {
            InitializeComponent();
            ObserveableProductList = new ObservableCollection<ProductItem>();

            //prodList = new List<fooItem>
            //{
            //    new fooItem
            //    {
            //        Id = "1",
            //        Name = "TestItem",
            //        Quantity = 3
            //    },
            //    new fooItem
            //    {
            //        Id = "2",
            //        Name = "TestItem2",
            //        Quantity = 3
            //    },
            //    new fooItem
            //    {
            //        Id = "3",
            //        Name = "TestItem3",
            //        Quantity = 3
            //    },
            //};


            //getLoadedProductList(); //reduced loading time by pushing the process on the splash screen
            try
            {
                if(GlobalValues.Manager_Product_save == null)
                {
                    getProductList();//retry loading product since it is not downloaded
                }else
                {
                    ProcessProdList(GlobalValues.Manager_Product_save);
                }
               
            }catch(Exception ex)
            {
                DisplayAlert("Sorry about that", "*Jayward is fixing the problem \n\n+" + ex.Message, "Ok");
            }

            //getProductList();
            //listItem = new fooItemList();
            //listItem.ItemList(prodList);

            productList.ItemsSource = ObserveableProductList;
            productList.ItemTapped += ProductList_ItemTapped;
            //productList.RowHeight += 50;
            productList.HasUnevenRows = true;
        }

        private async void  ProductList_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            var ProductItemObject = (ProductItem)e.Item;
            ProductItem SpecificProductItem = (from currItem in ObserveableProductList
                                  where currItem.Id == ProductItemObject.Id.ToString()
                                  select currItem).FirstOrDefault<ProductItem>();

            newView = new viewProduct(SpecificProductItem.Id, 
                SpecificProductItem.Name, 
                SpecificProductItem.Price, 
                SpecificProductItem.Quantity);

            newView.Disappearing += (o, ea) =>
            {
                if (ProductItemObject.Quantity != newView.NewQuantity)
                {
                    int x = ObserveableProductList.IndexOf(ProductItemObject);
                    ProductItem TemporaryProduct = ObserveableProductList[x];
                    ObserveableProductList.Remove(TemporaryProduct);
                    TemporaryProduct.Quantity = newView.NewQuantity;

                    ObserveableProductList.Add(TemporaryProduct);
                    ObserveableProductList.Move(ObserveableProductList.Count - 1, 0);
                    //ObserveableProductList[x].Quantity = newView.NewQuantity;
                }
               
                //newView = null;
            };
            await Navigation.PushModalAsync(newView);

            
                

            productList.SelectedItem = null;
        }

        //public void EditClicked (object sender, EventArgs e)
        //{
        //    var item = (Xamarin.Forms.Button)sender;
        //    ProductItem lvItem = (from currItem in prodList
        //                      where currItem.Id == item.CommandParameter.ToString()
        //                      select currItem).FirstOrDefault<ProductItem>();


        //    Navigation.PushModalAsync(new viewProduct(lvItem.Id, lvItem.Name, lvItem.Price, lvItem.Quantity));
        //}


        public async void getProductList()
        {
            ProductList listProd = new ProductList();
            try
            {
                JsonValue prodList = await listProd.getProducttList();
                ProcessProdList(prodList);
            }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", "Invalid List: \n" + ex.Message, "OK");
            }
        }

        //public void getLoadedProductList()
        //{
        //    try
        //    {
        //        ProcessProdList(GlobalValues.Manager_Product_save);
        //    }
        //    catch (Exception ex)
        //    {
        //        DisplayAlert("Alert", "Invalid List" + ex.Message, "OK");
        //    }

        //}


        public void ProcessProdList(JsonValue json)
        {
            // Get the weather reporting fields from the layout resource: 

            //TextView location = FindViewById<TextView>(Resource.Id.locationText);
            //TextView temperature = FindViewById<TextView>(Resource.Id.tempText);
            //TextView humidity = FindViewById<TextView>(Resource.Id.humidText);

            //lbl_name.Text = json["ProductName"];

            //double Price = json["Price"];
            //lbl_price.Text = String.Format("{0:C}", Price);

            //double Quantity = json["Quantity"];
            //lbl_qty.Text = Quantity.ToString();

            for (int x = 0; x < json.Count; x++)
            {
                //fooItem getItem = new fooItem();
                //getItem.Name = json[x]["ProductName"];
                //getItem.Price = json[x]["Price"];
                //getItem.Quantity = json[x]["Quantity"];
                double prodPrice = json[x]["Price"];
                int prodQuantity = json[x]["Quantity"];

                string name = json[x]["ProductName"].ToString().Length > GlobalValues.MAX_ADMIN_STRING_LENGTH ?
                    json[x]["ProductName"].ToString().Remove(GlobalValues.MAX_ADMIN_STRING_LENGTH) + "..." :
                    json[x]["ProductName"].ToString();

                ObserveableProductList.Add(new ProductItem
                {
                    Id = json[x]["Id"].ToString(),
                    Name = name,
                    Price = prodPrice,
                    Quantity = prodQuantity
                });
                //double prodPrice = json[x]["Price"];
                //int prodQuantity = json[x]["Quantity"];
                //prodList = new List<fooItem>
                //{
                //    new fooItem
                //    {
                //        Name = json[x]["ProductName"],
                //        Price = prodPrice,
                //        Quantity = prodQuantity
                //    }
                //};
            }
        }
    }
}
