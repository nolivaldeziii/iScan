﻿using System;
using System.Collections.Generic;
using System.Json;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iScan.Manager.Dependency
{
    public interface IGetProducts
    {
        Task<JsonValue> getProducttList();
    }
}
