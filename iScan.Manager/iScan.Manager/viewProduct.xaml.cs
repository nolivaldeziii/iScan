﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer;
using Xamarin.Forms;

namespace iScan.Manager
{
    public partial class viewProduct : ContentPage
    {
        private int initQuantity;

        public int NewQuantity { get; private set; } 
        public viewProduct(string prodID, string prodName, double Price, int Quantity)
        {
            InitializeComponent();

            initQuantity = Quantity;
            try
            {
                NewQuantity = Quantity;
                lbl_ID.Text = prodID;
                lbl_name.Text = prodName;
                lbl_price.Text = string.Format("{0:C}",Price);
                entry_Quantity.Text = Quantity.ToString();
                  
            }
            catch
            {
                throw new Exception("View Product Error");
            }

            btn_cancel.Clicked += ((o,ea)=> {
                Navigation.PopModalAsync();
            });
        }
        public async void updateClicked(object sender, EventArgs e)
        {
            SetProductQuantity setQuantity = new SetProductQuantity();
            NewQuantity = Convert.ToInt32(entry_Quantity.Text);
            bool good = false;
            try
            {
                await setQuantity.setProductQuantity(lbl_ID.Text, NewQuantity.ToString());
                good = true;
            }
            catch(System.Net.WebException ex)
            {
                await DisplayAlert("Connection problem", "Sorry, seems like we can't connect to iScan servers...\nPlease try again", "Ok");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Sorry", "*Jayward is fixing the problem \n\n"+ex.Message, "Ok");
            }
            finally
            {
                if(!good)
                    await Navigation.PopModalAsync();
            }
            //setQuantity.fetchProductQuantity(lbl_ID.Text, quantity);
            

            await DisplayAlert("Alert", string.Format("Updated item quantity for:\n\n   {0}\n\n Current quantity: {1}",
                lbl_name.Text, entry_Quantity.Text), "Ok");

            await Navigation.PopModalAsync();




        }
    }

   
}
