﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


using System.Net;
using System.IO;
using System.Json;

using BusinessLayer;
using Plugin.Connectivity;

namespace iScan
{
    public partial class MainPage : ContentPage
    {
        QRScanner ScannerAPI;
        public List<Cart> CartItems;
        

        public MainPage()
        {
            InitializeComponent();
            ScannerAPI = new QRScanner();
            CartItems = new List<Cart>();

            Run_QR_scannerView(null, null); // go to scanner immediately

            //btn_scan_item.Clicked += Run_QR_scannerView;
            img_scan_item.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    Run_QR_scannerView(null, null);
                }),
                NumberOfTapsRequired = 1
            });

            //btn_view_cart.Clicked += ViewCart;
            img_view_cart.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() => {
                    ViewCart(null, null);
                }),
                NumberOfTapsRequired = 1
            });

            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;

        }

       

        private void ViewCart(object sender, EventArgs e)
        {
            CartViewPage MyCartView = new CartViewPage(ref CartItems);



            Navigation.PushModalAsync(MyCartView);
        }

        private async void Run_QR_scannerView(object sender, EventArgs e)
        {
            DetailsView newView = new DetailsView(ref CartItems);
            var ScanResult = await ScannerAPI.Run_QR_scannerView(); // go to scanner immediately

            //redirect to details view 
            try
            {
                if (ScanResult.Text != null)
                {
                    ItemQR ScannedItem = new ItemQR(ScanResult.Text.ToUpper());

                    if (newView.DisplayOfflineData(ScannedItem))
                    {


                        await Navigation.PushModalAsync(newView);

                        if (CrossConnectivity.Current.IsConnected)
                        {
                            newView.SetStatus("Fetching data...", Color.Green, Color.White);

                            JsonValue ProdDetails = await ScannerAPI.GetProductDetailFromServer(((SingleItem)ScannedItem.Items[0]).Id.ToString());
                            newView.ParseAndDisplay(ProdDetails);
                            newView.ParseProductImage();
                            newView.HideStatus();
                        }
                        else
                        {
                            newView.SetStatus("Offline...", Color.Gray, Color.White);
                        }
                    }else
                    {
                        await DisplayAlert("Invalid QR", "You scanned a broken QR", "OK");
                    }

                }
            }
            catch (NullReferenceException)
            { 
                //this happens when user press back button, because nothing is scanned
            }
            catch (WebException)
            {
                newView.SetStatus("Server Connection Error",Color.Yellow,Color.White);
            }
            catch (System.ArgumentException)
            {
                await DisplayAlert("Invalid QR", "You scanned this QR: \n\n" + ScanResult.Text, "OK");
            }
            catch (Exception ex)
            {
                await DisplayAlert("Sorry", "An error has occured: \n *Jayward is fixing the problem* \n\n" + ex.Message, "OK");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            //await ChangeWarningMessage(!CrossConnectivity.Current.IsConnected);
            ChangeWarningMessage(CrossConnectivity.Current.IsConnected);
            
        }

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            ChangeWarningMessage(CrossConnectivity.Current.IsConnected);
        }

        private void ChangeWarningMessage(bool IsConnected)
        {
            if (IsConnected)
            {
                lbl_msg.BackgroundColor = Color.Green;
                lbl_msg.TextColor = Color.White;
                lbl_msg.IsVisible = false;
            }
            else
            {
                lbl_msg.BackgroundColor = Color.Gray;
                lbl_msg.TextColor = Color.White;
                lbl_msg.IsVisible = true;
            }
            
            lbl_msg.Text = (IsConnected) ? "Connected to Internet" : "Offline mode" ;
        }

    }
}
