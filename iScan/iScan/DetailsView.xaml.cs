﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Net;
using System.IO;
using System.Json;

using BusinessLayer;

namespace iScan
{
    public partial class DetailsView : ContentPage
    {
        public bool RedoScan { get; private set; }
        private List<Cart> TheCart;
        public Cart CurrentItem { get; private set; }

        private string CurrentItemImageURL;
        public DetailsView(ref List<Cart> TheCart)
        {
            InitializeComponent();

            this.TheCart = TheCart;
            this.RedoScan = false;

            btn_buy.Clicked += BuyCurrentItem;
        }

        private void BuyCurrentItem(object sender, EventArgs e)
        {
            bool Existing = false;
            foreach(Cart i in TheCart)
            {
                if(i.Id == CurrentItem.Id)
                {
                    i.Quantity = (Convert.ToInt32(i.Quantity)+1).ToString();
                    Existing = true;
                    break;
                }
            }
            if (!Existing)
            {
                CurrentItem.Quantity = (1).ToString();
                TheCart.Add(CurrentItem);
            }

            Navigation.PopModalAsync();
        }

        public bool DisplayOfflineData(ItemQR QR)
        {
            if(QR.Type != QRTYPE.Item)
            {
                //show alert modal and exit view
                // or throw an exception
                //DisplayAlert("Alert", "You scanned an invalid QR", "OK");
                Navigation.PopModalAsync();
                return false;
            }

            lbl_pid.Text        = ((SingleItem)QR.Items[0]).Id;
            lbl_name.Text       = ((SingleItem)QR.Items[0]).Name.ToLower();
            lbl_price.Text      = "loading...";
            lbl_qty.Text        = "0";
            lbl_info.Text       = ((SingleItem)QR.Items[0]).Info.ToLower();

            CurrentItem = new Cart {
                Id = ((SingleItem)QR.Items[0]).Id,
                Quantity = (1).ToString(),
                Name = ((SingleItem)QR.Items[0]).Name.ToLower()
            };
            return true;
        }

        public void ParseAndDisplay(JsonValue json)
        {

            CurrentItem.Name    = json["ProductName"];
            lbl_name.Text       = string.Format("{0}", json["ProductName"]);

            double Price        = json["Price"];
            CurrentItem.Price   = Price;
            lbl_price.Text = String.Format("{0:C}", Price);

            CurrentItemImageURL = json["Image"].ToString().Trim('"');

            double Quantity     = json["Quantity"];
            lbl_qty.Text = string.Format("{0}", Quantity.ToString());

            lbl_info.Text       = json["Description"];

            btn_buy.IsEnabled = true;
        }

        public void ParseProductImage()
        {
            lbl_img_status.IsVisible = true;
            img_product.Source = ImageSource.FromUri(new Uri(CurrentItemImageURL));
            lbl_img_status.IsVisible = false;
        }
        public void SetStatus(string msg,Color bgc,Color tc)
        {
            lbl_status.IsVisible = true;
            lbl_status.Text = msg;
            lbl_status.BackgroundColor = bgc;
            lbl_status.TextColor = tc;
        }

        public void HideStatus()
        {
            lbl_status.Text = "";
            lbl_status.IsVisible = false;
        }

    }
}
