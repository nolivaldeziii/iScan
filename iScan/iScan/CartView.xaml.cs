﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

using System.Drawing;
using Android.Graphics;

using BusinessLayer;
using System.Collections.ObjectModel;

namespace iScan
{
    public partial class CartViewPage : ContentPage
    {
        
        List<Cart> TheCart;
        ObservableCollection<Cart> ObserveableCart;
        double CartTotal;
        public CartViewPage(ref List<Cart> CartItems)
        {
            InitializeComponent();
            TheCart = CartItems;
            CartTotal = 0;


            ObserveableCart = new ObservableCollection<Cart>();
            

            CartViewer.ItemsSource = ObserveableCart;
            btn_gen_qr.Clicked += Btn_gen_qr_Clicked;

            //var src = QRGenerator.GenerateQRToStream("hello", 200, 600, 5);
            //qr_img.Source = src;
            CartViewer.ItemTapped += CartViewer_ItemTapped;
            //CartViewer.ItemSelected += CartViewer_ItemSelected;

            CartViewer.HasUnevenRows = true;

            this.Focused += ((o, ea) => RefreshData());
            this.Appearing += ((o, ea) => RefreshData());

            RefreshData();
        }

        private void CartViewer_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Cart SelectedCartItem = (Cart)e.Item;
            EditView newEdit = new EditView(ref TheCart, SelectedCartItem.Id);

            Navigation.PushModalAsync(newEdit);
        }

        private void Btn_gen_qr_Clicked(object sender, EventArgs e)
        {
            // string QR_STRING = @"{""Type"": ""Item"", ""Items"": [{""Id"": ""1"", ""Name"": ""Product Name"",""Product Info"": ""Lorem Ipsum""}  ] }";

            string QR_STRING = BusinessLayer.ItemQR.GenerateCartJSON(TheCart);

            DependencyService.Get<iScan.Dependency.IBitmap>().GenerateQRImage(QR_STRING);
        }

        
        private void RefreshData()
        {
            CartTotal = 0;
            ObserveableCart.Clear();
            foreach (Cart i in TheCart)
            {
                if(i.Name.Length > GlobalValues.MAX_CART_STRING_LENGTH)
                {
                    i.Name = i.Name.Remove(GlobalValues.MAX_CART_STRING_LENGTH) +"...";
                }
                i.Price = i.Price * Convert.ToInt32(i.Quantity);
                ObserveableCart.Add(i);
                CartTotal += i.Price * Convert.ToInt32(i.Quantity);
            }
            lbl_carttotal.Text = string.Format("{0:C}", CartTotal);

            CartViewer.SelectedItem = null;
            if(TheCart.Count <= 0)
            {
                btn_gen_qr.IsEnabled = false;
            }

        }

        
    }
}
