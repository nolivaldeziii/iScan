﻿using Android.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace iScan.Dependency
{
    public interface IBitmap
    {
        void GenerateQRImage(string data);
    }


}
