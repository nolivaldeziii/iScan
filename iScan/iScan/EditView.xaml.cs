﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace iScan
{
    public partial class EditView : ContentPage
    {
        private List<BusinessLayer.Cart> TheCart;
        private int CartItemIndex;
        public EditView(ref List<BusinessLayer.Cart> TheCart, string pid)
        {
            InitializeComponent();
            CartItemIndex = -1;
            slider_qty.Maximum = 10;
            slider_qty.Minimum = 1.0;
            
            this.TheCart = TheCart;

            for (int i = 0; i < TheCart.Count; i++)
            {
                if (TheCart[i].Id == pid)
                {
                    CartItemIndex = i;
                    break;
                }
            }
            if (CartItemIndex == -1)
            {
                throw new Exception("Item Not FOund");
            }

            lbl_name.Text = TheCart[CartItemIndex].Name;
            lbl_quantity.Text = TheCart[CartItemIndex].Quantity;

            lbl_totalprice.Text = string.Format("{0:C}", (Convert.ToInt32(TheCart[CartItemIndex].Quantity) * Convert.ToInt32(TheCart[CartItemIndex].Price)));

            slider_qty.Value = Convert.ToDouble(Convert.ToInt32(TheCart[CartItemIndex].Quantity));
            slider_qty.ValueChanged += Slider_qty_ValueChanged;

            btn_remove.Clicked += Btn_remove_Clicked;
            btn_accept.Clicked += Btn_accept_Clicked;
        }

        private void Btn_accept_Clicked(object sender, EventArgs e)
        {
            Navigation.PopModalAsync();
        }

        private void Btn_remove_Clicked(object sender, EventArgs e)
        {
            TheCart.RemoveAt(CartItemIndex);
            Navigation.PopModalAsync();
        }

        private void Slider_qty_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            var newStep = Math.Round(e.NewValue / 1);
            slider_qty.Value = newStep * 1;
            TheCart[CartItemIndex].Quantity = Convert.ToInt32(slider_qty.Value).ToString();

            lbl_quantity.Text = TheCart[CartItemIndex].Quantity;
            lbl_totalprice.Text = string.Format("{0:C}", (Convert.ToInt32(TheCart[CartItemIndex].Quantity) * Convert.ToInt32(TheCart[CartItemIndex].Price)));
        }

        
    }
}
