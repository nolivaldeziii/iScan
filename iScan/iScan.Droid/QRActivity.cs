using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Android.Graphics;

namespace iScan.Droid
{
    [Activity(Label = "QR for Cashier")]
    public class QRActivity : Activity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            SetContentView(Resource.Layout.QRgeneratorUI);
            
            var BitmapStream = GenerateQRToStream(this.Intent.GetStringExtra("data"), 800, 800, 0);
            Bitmap img = BitmapFactory.DecodeStream(BitmapStream);



            ImageView imgv = FindViewById<ImageView>(Resource.Id.img_qr);
            imgv.SetImageBitmap(img);
            // Create your application here
        }

        public static Stream GenerateQRToStream(string data, int w, int h, int m)
        {
            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
                {
                    Width = w,
                    Height = h,
                    Margin = m
                }
            };

            barcodeWriter.Renderer = new ZXing.Mobile.BitmapRenderer();
            var bitmap = barcodeWriter.Write(data.ToUpper());
            var stream = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Png, 100, stream);  // this is the diff between iOS and Android
            stream.Position = 0;
            return stream;
        }
    }
}