using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Android.Graphics;
using Xamarin.Forms;

using iScan.Dependency;
using iScan.Droid;
using System.IO;

[assembly: Xamarin.Forms.Dependency(typeof(Bitmap_Android))]
namespace iScan.Droid
{
    public class Bitmap_Android : Java.Lang.Object, IBitmap
    {
        public void GenerateQRImage(string data)
        {
            //((Activity)Forms.Context).SetContentView(Resource.Layout.QRgeneratorUI);

            //var BitmapStream = GenerateQRToStream(data, 800,800,0);
            //Bitmap img = BitmapFactory.DecodeStream(BitmapStream);



            //ImageView imgv = ((Activity)Forms.Context).FindViewById<ImageView>(Resource.Id.img_qr);
            //imgv.SetImageBitmap(img);


            var intent = new Intent(Forms.Context, typeof(QRActivity));
            intent.PutExtra("data", data);
            ((Activity)Forms.Context).StartActivity(intent);

        }

        public static Stream GenerateQRToStream(string data, int w, int h, int m)
        {
            var barcodeWriter = new ZXing.Mobile.BarcodeWriter
            {
                Format = ZXing.BarcodeFormat.QR_CODE,
                Options = new ZXing.Common.EncodingOptions
                {
                    Width = w,
                    Height = h,
                    Margin = m
                }
            };

            barcodeWriter.Renderer = new ZXing.Mobile.BitmapRenderer();
            var bitmap = barcodeWriter.Write(data.ToUpper());
            var stream = new MemoryStream();
            bitmap.Compress(Bitmap.CompressFormat.Png, 100, stream);  // this is the diff between iOS and Android
            stream.Position = 0;
            return stream;
        }

        //public void DisplayImageByStream(System.IO.Stream data)
        //{
        //    ((Activity)Forms.Context).SetContentView(Resource.Layout.QRgeneratorUI);

        //    Bitmap img = bytesToUIImage(data);



        //    ImageView imgv = ((Activity)Forms.Context).FindViewById<ImageView>(Resource.Id.img_qr);
        //    imgv.SetImageBitmap(img);


        //}

        //public static Bitmap bytesToUIImage(Stream bytes)
        //{
        //    //if (bytes == null)
        //    //    return null;

        //    //Bitmap bitmap;


        //    //var documentsFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);

        //    ////Create a folder for the images if not exists
        //    //System.IO.Directory.CreateDirectory(System.IO.Path.Combine(documentsFolder, "images"));

        //    //string imatge = System.IO.Path.Combine(documentsFolder, "images", "image.jpg");


        //    //System.IO.File.WriteAllBytes(imatge, bytes.Concat(new Byte[] { (byte)0xD9 }).ToArray());

        //    //bitmap = BitmapFactory.DecodeFile(imatge);

        //    //Bitmap qrContent = BitmapFactory.DecodeByteArray(bytes, 0, bytes.Length);
        //    Bitmap qrContent = BitmapFactory.DecodeStream(bytes);
        //    return qrContent;
        //}

    }
}