﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;


using System.Net;
using System.IO;
using System.Json;

using BusinessLayer;
using Plugin.Connectivity;

namespace iScan.Cashier
{
    public partial class MainPage : ContentPage
    {
        QRScanner ScannerAPI;
        public List<Cart> CartItems;
        //public List<object> ProductInfo;

        public class ItemDetails
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Quantity { get; set; }
        }

        //int counter = 0;

        public MainPage()
        {
            InitializeComponent();
            ScannerAPI = new QRScanner();
            CartItems = new List<Cart>();

            //    Run_QR_scannerView(null, null); // go to scanner immediately

            img_scan_item.GestureRecognizers.Add(new TapGestureRecognizer
            {
                Command = new Command(() =>
                {
                    Run_QR_scannerView(null, null);
                }),
                NumberOfTapsRequired = 1
            });

            CrossConnectivity.Current.ConnectivityChanged += Current_ConnectivityChanged;
        }

        private async void Run_QR_scannerView(object sender, EventArgs e)
        {     
            var ScanResult = await ScannerAPI.Run_QR_scannerView(); // go to scanner immediately

            try
            {
                if (ScanResult.Text != null)
                {
                    ItemQR ScannedItem = new ItemQR(ScanResult.Text.ToUpper());

                    if (CrossConnectivity.Current.IsConnected)
                    {
                        //     newView.SetStatus("Fetching data...", Color.Green, Color.White);

                        foreach (object CartItem in ScannedItem.Items)
                        {
                            CartItems.Add(((Cart)CartItem));
                            //JsonValue ProdDetails = 
                            //    await ScannerAPI.GetProductDetailFromServer((
                            //    (Cart)ScannedItem.Items[counter]).Id.ToString());

                            //ProductInfo = new List<object>();
                            //ItemDetails itemdetails = new ItemDetails();
                            //itemdetails.Id = ((Cart)ScannedItem.Items[counter]).Id;
                            //itemdetails.Name = ((Cart)ScannedItem.Items[counter]).Name;
                            //itemdetails.Quantity = ((Cart)ScannedItem.Items[counter]).Quantity;
                            //ProductInfo.Add(itemdetails);

                            //newView.ParseAndDisplay(ProdDetails);

                            //counter++;
                        }
                        //await DisplayAlert("Alert", "success", "OKч" + counter);
                        CustomerCart newView = new CustomerCart(ref CartItems);
                        //newView.DisplayOfflineData(ScannedItem);
                        await Navigation.PushModalAsync(newView);
                        //  newView.HideStatus();
                    }
                    else
                    {
                        // newView.SetStatus("Offline...", Color.Gray, Color.White);
                    }

                }//end of if
            }//end of try
            catch (NullReferenceException) { }
            catch (Exception ex)
            {
                await DisplayAlert("Alert", "You scanned an invalid QR: \n" + ex.Message + Id, "OKй");
            }
        }

        private void Current_ConnectivityChanged(object sender, Plugin.Connectivity.Abstractions.ConnectivityChangedEventArgs e)
        {
            ChangeWarningMessage(CrossConnectivity.Current.IsConnected);
        }

        private void ChangeWarningMessage(bool IsConnected)
        {
            if (IsConnected)
            {
                lbl_msg.BackgroundColor = Color.Green;
                lbl_msg.TextColor = Color.White;
            }
            else
            {
                lbl_msg.BackgroundColor = Color.Red;
                lbl_msg.TextColor = Color.Black;
            }

            lbl_msg.Text = (IsConnected) ? "Connected to Internet" : "No Internet";
        }




    }
}
